// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code

// is governed by a BSD-style license that can be found in the LICENSE file.

/// jb_mailer
///
/// A web server.
library jb_mailer;

import 'dart:io';
import 'dart:async';
import 'package:aqueduct/aqueduct.dart';
import 'package:scribe/scribe.dart';
import 'package:mailer/mailer.dart';

export 'package:aqueduct/aqueduct.dart';
export 'package:scribe/scribe.dart';

part 'src/model/token.dart';

part 'src/model/user.dart';

part 'src/jb_mailer_sink.dart';

part 'src/controller/email_controller.dart';
