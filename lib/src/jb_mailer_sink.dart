part of jb_mailer;

class JbMailerConfiguration extends ConfigurationItem {
  JbMailerConfiguration(String fileName) : super.fromFile(fileName);

  int port;
}

class JbMailerSink extends RequestSink {
  static const String ConfigurationKey = "ConfigurationKey";
  static const String LoggingTargetKey = "LoggingTargetKey";

  JbMailerSink(Map<String, dynamic> opts) : super(opts) {
    configuration = opts[ConfigurationKey];

    LoggingTarget target = opts[LoggingTargetKey];
    target?.bind(logger);

  }

  ManagedContext context;
  JbMailerConfiguration configuration;

  @override
  void setupRouter(Router router) {
    router.route("/email").generate(() => new EmailController());
  }

}
