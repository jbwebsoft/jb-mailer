part of jb_mailer;

class EmailController extends HTTPController {


  //TODO: send meaningfull body later
  @httpPost
  Future<Response> sendEmail(@HTTPHeader("origin") String origin) async {
    //load configuration of smtp server by the domain where this request came from (fewo-birkennest.de, fliesenleger-lieberwirth.de ...)

    String originInternal = request.innerRequest.headers.value("origin");

    String username = "noreply@fewo-birkennest.de";
    String password = "k3zS3gBYm765LKLk";
    String hostname = "smtp.strato.de";

    String targetAddress = "bjesuiter@gmail.com";

    var options = new SmtpOptions()
      ..username = username
      ..password = password
      ..hostName = hostname
      ..port = 465
      ..requiresAuthentication = true
      ..secured = true;

    var emailTransport = new SmtpTransport(options);

    var envelope = new Envelope()
      ..from = username
      ..recipients.add(targetAddress)
      ..subject = "Testing the Dart Mailer Library"
      ..text = "Cool Text?"
      ..html = "<h1>Cool HTML with ${new DateTime.now()} !</h1>";

    try {
      Envelope response = await emailTransport.send(envelope);
    } catch (e) {
      //TODO: add Error to Aqueduct logging
      print(e);
      printDebugInformation();
    }

    return new Response.ok("OK");
  }

  EmailController() {
//    if (policy.allowedOrigins == null) policy.allowedOrigins = [];
    policy.allowedOrigins = [];

    policy.allowedOrigins
      ..add("http://fliesenleger-lieberwirth.de")
      ..add("http://ib-lieberwirth.de")
      ..add("http://komplettbau-gmbh-lieberwirth.de")
      ..add("http://bauwerkssanierung-stollberg.de")
      ..add("http://fewo-birkennest.de");
  }
}
